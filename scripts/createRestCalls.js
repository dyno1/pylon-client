const fs = require('fs');
const path = require('path');
const { argv } = require('process');

const dtspath = path.resolve('./node_modules/@pylonbot/pylon-gateway-protobuf/dist/gateway/v1/rest_service.d.ts')

let typings = fs.readFileSync(dtspath, 'utf-8');

const marker = 'export interface GatewayRestClient extends Client {';
typings = typings.substr(typings.indexOf(marker) + marker.length);
typings = typings.substr(0, typings.indexOf('}'));

let functions = typings.split('\n');

functions = functions.map(str => str.trim().replace('\r', ''));

const functionNameAndType = new Map();

functions = functions.forEach(str => {
    let match = /(.+?)\(request: (.+?),/.exec(str);
    if(!match) return null;
    const [_, funcName, type] = match;

    functionNameAndType.set(funcName, type);
});


function createFunction(name, grpcType) {
    return `\tasync ${name}(request: PylonRest.DeepPartial<PylonRest.${grpcType}>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.${grpcType}Transform(await this.promisifyUnaryCall<PylonRest.${grpcType.replace("Request", "Response")}>(this._restClient.${name}.bind(this._restClient), PylonRest.${grpcType}.fromPartial(request), metadata)) };`
}


let functionStr = "";
for(let kv of functionNameAndType) {
    let [key, value] = kv;
    functionStr += createFunction(key, value) + "\n";
}


const restClientPath = path.resolve('./src/rest/restClient.ts')
let restClientText = fs.readFileSync(restClientPath, 'utf-8');

const startMarker = "//{{marker_start}}";
const endMarker = "//{{marker_end}}";

let modifiedRestClient = "";
modifiedRestClient += restClientText.substr(0, restClientText.indexOf(startMarker) + startMarker.length);

modifiedRestClient += restClientText.substr(restClientText.indexOf(endMarker));

modifiedRestClient = modifiedRestClient.replace("//{{marker_start}}//{{marker_end}}", `//{{marker_start}}\n${functionStr}\t//{{marker_end}}`)

fs.writeFileSync(restClientPath, modifiedRestClient)