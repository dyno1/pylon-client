import { RestClient, CacheClient, GatewayAPIClient } from '.';
import { PylonMetadata } from './structures/PylonMetadata';

const botId = '347378555296022558';
const restClient = new RestClient('10.12.4.4:32001', botId);
const cacheClient = new CacheClient('10.12.4.4:32001', botId);
const gwApiClient = new GatewayAPIClient('10.12.4.4:32001', botId);

const objMeta : PylonMetadata = {
  guildId: "203039963636301824",
  // shardKey: "347378555296022558-0-1"
}
new Promise(async () => {
    const b1 = async () => { 
        console.time('listGuildMembers');
        console.log((await cacheClient.listGuildMembers({}, objMeta)).length);
        console.timeEnd('listGuildMembers');
    };

    const b2 = async () => {
        const count = 500;
        console.log(`Fetching ${count} ids`)
        console.time('getGuildMember');
        const ids = new Array(count).fill("253233185800847361");
        const promises = Promise.all(ids.map(id => cacheClient.getGuildMember({userId: id}, objMeta)));
        await promises;
        console.timeEnd('getGuildMember');
    }

    await b1();
    await b2();
});
