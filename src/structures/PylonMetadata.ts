export interface PylonMetadata {
    botId?: string,
    guildId?: string,
    maxRatelimitMs?: number,
    shardKey? : string,
}