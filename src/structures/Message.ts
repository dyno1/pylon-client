import {
	MessageData,
	MessageData_MessageActivityData,
	MessageData_MessageApplicationData,
	MessageData_MessageAttachmentData,
	MessageData_MessageEmbedData,
	MessageData_MessageMentionChannelData,
	MessageData_MessageMentionData,
	MessageData_MessageReactionData,
	MessageData_MessageReferenceData,
	MessageData_MessageType
} from "@pylonbot/pylon-gateway-protobuf/dist/discord/v1/model";

import Member from "./Member";
import User from "./User";

export default class Message {
	readonly id: string;
	readonly channelId: string;
	readonly guildId?: string;
	readonly content: string;
	readonly timestamp?: Date;
	readonly editedTimestamp?: Date;
	readonly mentionRoles?: string[];
	readonly tts: boolean;
	readonly mentionEveryone: boolean;
	readonly attachments: MessageData_MessageAttachmentData[];
	readonly embeds: MessageData_MessageEmbedData[];
	readonly mentions: MessageData_MessageMentionData[];
	readonly reactions: MessageData_MessageReactionData[];
	readonly pinned: boolean;
	readonly type: MessageData_MessageType;
	readonly mentionChannels: MessageData_MessageMentionChannelData[];
	readonly flags: number;
	readonly activity?: MessageData_MessageActivityData;
	readonly application?: MessageData_MessageApplicationData;
	readonly messageReference?: MessageData_MessageReferenceData;
	readonly author?: User;
	readonly member?: Member;
	readonly webhookId?: string;
	readonly _raw : MessageData;

	constructor(message: MessageData) {
		this._raw = message;
		this.id = message.id;
		this.channelId = message.channelId;
		if (message.guildId != undefined) {
			this.guildId = message.guildId?.value ?? message.guildId;
		}
		this.content = message.content;
		this.timestamp = message.timestamp;
		this.editedTimestamp = message.editedTimestamp;
		if (message.mentionRoles != undefined) {
			this.mentionRoles = message.mentionRoles.values;
		}
		this.tts = message.tts;
		this.mentionEveryone = message.mentionEveryone;
		this.attachments = message.attachments;
		this.embeds = message.embeds;
		this.mentions = message.mentions;
		this.reactions = message.reactions;
		this.pinned = message.pinned;
		this.type = message.type;
		this.mentionChannels = message.mentionChannels;
		this.flags = message.flags;
		if (message.activity != undefined) {
			this.activity = message.activity;
		}
		if (message.application != undefined) {
			this.application = message.application;
		}
		if (message.messageReference != undefined) {
			this.messageReference = message.messageReference;
		}
		if (message.author != undefined) {
			this.author = new User(message.author);
		}
		if (message.member != undefined) {
			this.member = new Member(message.member);
		}
		if (message.webhookId != undefined) {
			this.webhookId = message.webhookId?.value;
		}
	}

	get cleanContent() {
        let cleanContent = this.content && this.content.replace(/<a?(:\w+:)[0-9]+>/g, "$1") || "";

        if(this.mentions) {
            this.mentions.forEach((mention) => {
                cleanContent = cleanContent.replace(new RegExp(`<@!?${mention.id}>`, "g"), "@\u200b" + mention.username);
            });
        }

        if(this.guildId && this.mentionRoles) {
            for(const roleID of this.mentionRoles) {
                cleanContent = cleanContent.replace(new RegExp(`<@&${roleID}>`, "g"), "<@&\u200b" + roleID + ">");
			}
			
        }

        this.mentionChannels.forEach((channel) => {
            if(channel && channel.name) {
                cleanContent = cleanContent.replace(`<#${channel.id}>`, "#" + channel.name);
            }
        });

        return cleanContent.replace(/@everyone/g, "@\u200beveryone").replace(/@here/g, "@\u200bhere");
    }
}