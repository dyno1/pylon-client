import { ChannelData, ChannelData_ChannelPermissionOverwriteData, ChannelData_ChannelType, SnowflakeValue } from "@pylonbot/pylon-gateway-protobuf/dist/discord/v1/model";

export default class Channel {
    public id: string;
    public guildId?: string | undefined;
    public name: string;
    public topic: string | undefined;
    public type: ChannelData_ChannelType;
    public nsfw: boolean;
    public position: number;
    public bitrate: number;
    public userLimit: number;
    public parentId: SnowflakeValue | undefined;
    public rateLimitPerUser: number;
    public permissionOverwrites: ChannelData_ChannelPermissionOverwriteData[];
    constructor(channelData : ChannelData) {
        this.id = channelData.id;
        this.guildId = channelData.guildId?.value;
        this.name = channelData.name;
        this.topic = channelData.topic;
        this.type = channelData.type;
        this.nsfw = channelData.nsfw;
        this.position = channelData.position;
        this.bitrate = channelData.bitrate;
        this.userLimit = channelData.userLimit;
        this.parentId = channelData.parentId;
        this.rateLimitPerUser = channelData.rateLimitPerUser;
        this.permissionOverwrites = channelData.permissionOverwrites;
    }
}