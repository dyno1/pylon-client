import { UserData } from "@pylonbot/pylon-gateway-protobuf/dist/discord/v1/model";

export default class User {
	readonly user: UserData;
	readonly id: string;
	readonly username: string;
	readonly discriminator: number;
	readonly avatar?: string;
	readonly bot?: boolean;

	constructor(user: UserData) {
		this.user = user;
		this.id = user.id;
		this.username = user.username;
		this.discriminator = user.discriminator;

		if (user.avatar != undefined) {
			this.avatar = user.avatar;
		}

		if (user.bot != undefined) {
			this.bot = user.bot;
		}
	}

	get avatarURL() {
		return 'https://cdn.discordapp.com' + (this.avatar ? this.userAvatar(this.id, this.avatar) : this.defaultAvatar(this.discriminator));
	}

	get mention() {
		return `<@!${this.id}>`;
	}

	public defaultAvatar(userDiscriminator: number): string {
		return `/embed/avatars/${userDiscriminator}.png`;
	}

	public userAvatar(userID: string, userAvatar: string): string {
		return userAvatar.startsWith('a_') ? `/avatars/${userID}/${userAvatar}.gif` : `/avatars/${userID}/${userAvatar}.png`;
	}
}