import Permissions from './Permissions';

export default class Permission {
	public allow: bigint;
	public deny: bigint;

	constructor(allow: string|number, deny: string|number = 0) {
		this.allow = BigInt(allow);
		this.deny = BigInt(deny);
	}

	has(permission: string) {
        return !!(this.allow & Permissions[permission]);
    }

    toString() {
        return `[${this.constructor.name} +${this.allow} -${this.deny}]`;
    }
}
