const Permissions: {[key: string]: bigint} = {
    createInstantInvite:  1n,
    kickMembers:          1n << 1n,
    banMembers:           1n << 2n,
    administrator:        1n << 3n,
    manageChannels:       1n << 4n,
    manageGuild:          1n << 5n,
    addReactions:         1n << 6n,
    viewAuditLog:         1n << 7n, viewAuditLogs: 1n << 7n, // [DEPRECATED]
    voicePrioritySpeaker: 1n << 8n,
    voiceStream:          1n << 9n, stream: 1n << 9n, // [DEPRECATED]
    viewChannel:          1n << 10n, readMessages: 1n << 10n, // [DEPRECATED]
    sendMessages:         1n << 11n,
    sendTTSMessages:      1n << 12n,
    manageMessages:       1n << 13n,
    embedLinks:           1n << 14n,
    attachFiles:          1n << 15n,
    readMessageHistory:   1n << 16n,
    mentionEveryone:      1n << 17n,
    useExternalEmojis:    1n << 18n, externalEmojis: 1n << 18n, // [DEPRECATED]
    viewGuildInsights:    1n << 19n,
    voiceConnect:         1n << 20n,
    voiceSpeak:           1n << 21n,
    voiceMuteMembers:     1n << 22n,
    voiceDeafenMembers:   1n << 23n,
    voiceMoveMembers:     1n << 24n,
    voiceUseVAD:          1n << 25n,
    changeNickname:       1n << 26n,
    manageNicknames:      1n << 27n,
    manageRoles:          1n << 28n,
    manageWebhooks:       1n << 29n,
    manageEmojis:         1n << 30n,
    useSlashCommands:     1n << 31n,
    voiceRequestToSpeak:  1n << 32n
};

export default Permissions;