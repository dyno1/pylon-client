import { MemberData } from "@pylonbot/pylon-gateway-protobuf/dist/discord/v1/model";
import Permission from './Permission';
import User from "./User";

export default class Member {
	readonly member: MemberData;
	readonly id: string;
	readonly guildId: string;
	readonly user?: User;
	readonly nick?: string;
	readonly roles?: string[];
	readonly joinedAt?: Date;
	readonly premiumSince?: Date;
	readonly pending?: boolean;

	constructor(member: MemberData) {
		this.member = member;
		
		this.id = member.id;
		this.guildId = member.guildId;

		if (member.user != undefined) {
			this.user = new User(member.user);
		}
		if (member.nick != undefined) {
			this.nick = member.nick;
		}
		if (member.roles != undefined) {
			this.roles = member.roles;
		}
		if (member.joinedAt != undefined) {
			this.joinedAt = member.joinedAt;
		}
		if (member.premiumSince != undefined) {
			this.premiumSince = member.premiumSince;
		}
		if (member.pending != undefined) {
			this.pending = member.pending;
		}
	}

	get username() {
		return this.user?.username;
	}

	get discriminator() {
		return this.user?.discriminator;
	}

	get avatar() {
		return this.user?.avatar;
	}
	
	get avatarURL() {
		return this.user?.avatarURL;
	}

	get mention() {
		return `<@!${this.id}>`;
	}
	
	get permissions() {
		return new Permission(this.member.permissions);
	}
}
