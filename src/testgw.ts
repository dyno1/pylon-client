import { EventEnvelope, EventScope, MessageCreateEvent } from '@pylonbot/pylon-gateway-protobuf/dist/discord/v1/event';
import { MessageData } from '@pylonbot/pylon-gateway-protobuf/dist/discord/v1/model';
import { CreateMessageRequest, CreateMessageResponse } from '@pylonbot/pylon-gateway-protobuf/dist/discord/v1/rest';
import GatewayClient from './gateway/GatewayClient';
import RestClient from './rest/restClient';
import { PylonMetadata } from './structures/PylonMetadata';

class Dyno {
	public restClient: RestClient;
	public client: GatewayClient;

	constructor() {
		this.restClient = new RestClient('10.12.4.4:32001', '347378555296022558');
		this.client = new GatewayClient(
			'pylon://auth-token@pylon-router-endpoint/worker-group-id',
			{
				host: 'kubenode1',
				port: 32002,
				events: ['MESSAGE_CREATE'],
				meta: {
					'x-pylon-shard-key': '347378555296022558-0-1'
				},
			}
		);

		this.client.on('debug', console.log);
		this.client.on('info', console.log);
		this.client.on('error', console.error);
	
		// crude event handler that works off the raw proto structs
		this.client.on('event', (event: EventEnvelope) => {
			switch (event.eventData?.$case) {
				case "messageCreateEvent":
				const { messageData } = event.eventData.messageCreateEvent;
				if (messageData) {
					// console.log(util.inspect(event, { depth: 99 }));
					this.messageCreate(event.eventData.messageCreateEvent);
				}
				break;
			}
		});
		this.client.connect();
	}

	public async messageCreate({ scope, messageData }: MessageCreateEvent) {
		if (!scope || !messageData) {
			return;
		}

		const objMeta : PylonMetadata = {
			botId: scope?.botId,
			guildId: scope?.guildId,
			shardKey: `${scope?.botId}-0-1`,
		};

		const content = messageData?.content;

		if (content?.startsWith(';;;')) {
			const args = content.split(' ');
			const command = args.shift()?.replace(';;;', '');

			switch (command) {
				case 'ping': {
					const start = Date.now();

					this.restClient.createMessage({
						channelId: messageData.channelId,
						content: 'Pong!',
					}, objMeta).then(async (message) => {
						if (!message) { return; }
						const diff = (Date.now() - start);

						try {
							const { response: res } = await this.restClient.editMessage({
								channelId: messageData?.channelId,
								messageId: message.id,
								content: `Pong! ${diff}ms`,
							}, objMeta);
						} catch (err) {
							console.error(err);
						}
					});
				}
			}
		}

		console.log(`new message from ${messageData.author?.username}: ${messageData.content}`);
	}
};

new Dyno();
