import CacheClient from "./cache/cacheClient";
import GatewayAPIClient from "./gateway/GatewayAPIClient";
import RestClient from "./rest/restClient";
import { PylonMetadata } from "./structures/PylonMetadata";

new Promise(async (r) => {
  const botId = '347378555296022558';
  const restClient = new RestClient('10.12.4.4:32001', botId);
  const cacheClient = new CacheClient('10.12.4.1:32001', botId);
  const gwApiClient = new GatewayAPIClient('10.12.4.4:32001', botId);

  const objMeta : PylonMetadata = {
    guildId: "203039963636301824",
    // shardKey: "347378555296022558-0-1"
  }

  // const data = await restClient.getUser({ userId: "227115752396685313" }, objMeta);
  // console.log(data);

  // const data2 = await cacheClient.getGuild({}, objMeta);
  // console.log(data2);

  // const data3 = await cacheClient.getGuildChannel({channelId: "239446877953720322"}, objMeta);
  // console.log(data3);

  // const data4 = await gwApiClient.getStats({});
  // console.log(data4);

  // const data5 = await restClient.createDm({ recipientId: "227115752396685314" }).catch(console.log);
  // console.log(data5);
  // const data6 = await restClient.createMessage({ channelId: data5.response.data.channel?.id, content: 'hi' }).catch(console.log);

  const data7 = await cacheClient.findGuildMembers({ prefix: "227115752396685313", limit: 10 }, objMeta);
  console.log(data7);
  
  const data8 = await cacheClient.getGuildMember({ userId: "227115752396685313" }, objMeta);
  // console.log(data8);
});
