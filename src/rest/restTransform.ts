import * as PylonRest from "@pylonbot/pylon-gateway-protobuf/dist/discord/v1/rest";
import {
    Empty
} from "@pylonbot/pylon-gateway-protobuf/dist/google/protobuf/empty";
import {
    Message
} from "..";

export interface RestResponse<T> {
    $type: string,
    response?: {

        $case: "error";
        error: PylonRest.RestError;
    } | {

        $case: "data";
        data: T;

    };
}

export interface DataRestResponse<T> {
    $type: string,
    response: {

        $case: "data",
        data: T,
    }
}

export default class RestTransform {
    handleError<K, T extends RestResponse<K>>(data: T): DataRestResponse<K> {

        if (data.response?.$case === "data") {

            return <DataRestResponse<K>>data;
        }
        if (data.response?.$case === "error") {

            throw data.response.error;
        }
        throw new Error("$case was not 'data' | 'error'");
    }
    ModifyGuildRequestTransform(type: PylonRest.ModifyGuildResponse) {
        return this.handleError<PylonRest.ModifyGuildResponse_Data, PylonRest.ModifyGuildResponse>(type)
    };
    CreateGuildChannelRequestTransform(type: PylonRest.CreateGuildChannelResponse) {
        return this.handleError<PylonRest.CreateGuildChannelResponse_Data, PylonRest.CreateGuildChannelResponse>(type)
    };
    ModifyGuildChannelPositionsRequestTransform(type: PylonRest.ModifyGuildChannelPositionsResponse) {
        return this.handleError<Empty, PylonRest.ModifyGuildChannelPositionsResponse>(type)
    };
    AddGuildMemberRequestTransform(type: PylonRest.AddGuildMemberResponse) {
        return this.handleError<PylonRest.AddGuildMemberResponse_Data, PylonRest.AddGuildMemberResponse>(type)
    };
    ModifyGuildMemberRequestTransform(type: PylonRest.ModifyGuildMemberResponse) {
        return this.handleError<Empty, PylonRest.ModifyGuildMemberResponse>(type)
    };
    ModifyCurrentUserNickRequestTransform(type: PylonRest.ModifyCurrentUserNickResponse) {
        return this.handleError<Empty, PylonRest.ModifyCurrentUserNickResponse>(type)
    };
    AddGuildMemberRoleRequestTransform(type: PylonRest.AddGuildMemberRoleResponse) {
        return this.handleError<Empty, PylonRest.AddGuildMemberRoleResponse>(type)
    };
    RemoveGuildMemberRoleRequestTransform(type: PylonRest.RemoveGuildMemberRoleResponse) {
        return this.handleError<Empty, PylonRest.RemoveGuildMemberRoleResponse>(type)
    };
    RemoveGuildMemberRequestTransform(type: PylonRest.RemoveGuildMemberResponse) {
        return this.handleError<Empty, PylonRest.RemoveGuildMemberResponse>(type)
    };
    GetGuildBansRequestTransform(type: PylonRest.GetGuildBansResponse) {
        return this.handleError<PylonRest.GetGuildBansResponse_Data, PylonRest.GetGuildBansResponse>(type)
    };
    GetGuildBanRequestTransform(type: PylonRest.GetGuildBanResponse) {
        return this.handleError<PylonRest.GetGuildBanResponse_Data, PylonRest.GetGuildBanResponse>(type)
    };
    CreateGuildBanRequestTransform(type: PylonRest.CreateGuildBanResponse) {
        return this.handleError<Empty, PylonRest.CreateGuildBanResponse>(type)
    };
    RemoveGuildBanRequestTransform(type: PylonRest.RemoveGuildBanResponse) {
        return this.handleError<Empty, PylonRest.RemoveGuildBanResponse>(type)
    };
    CreateGuildRoleRequestTransform(type: PylonRest.CreateGuildRoleResponse) {
        return this.handleError<PylonRest.CreateGuildRoleResponse_Data, PylonRest.CreateGuildRoleResponse>(type)
    };
    ModifyGuildRolePositionsRequestTransform(type: PylonRest.ModifyGuildRolePositionsResponse) {
        return this.handleError<PylonRest.ModifyGuildRolePositionsResponse_Data, PylonRest.ModifyGuildRolePositionsResponse>(type)
    };
    ModifyGuildRoleRequestTransform(type: PylonRest.ModifyGuildRoleResponse) {
        return this.handleError<PylonRest.ModifyGuildRoleResponse_Data, PylonRest.ModifyGuildRoleResponse>(type)
    };
    DeleteGuildRoleRequestTransform(type: PylonRest.DeleteGuildRoleResponse) {
        return this.handleError<Empty, PylonRest.DeleteGuildRoleResponse>(type)
    };
    GetGuildPruneCountRequestTransform(type: PylonRest.GetGuildPruneCountResponse) {
        return this.handleError<Empty, PylonRest.GetGuildPruneCountResponse>(type)
    };
    BeginGuildPruneRequestTransform(type: PylonRest.BeginGuildPruneResponse) {
        return this.handleError<Empty, PylonRest.BeginGuildPruneResponse>(type)
    };
    GetGuildVoiceRegionsRequestTransform(type: PylonRest.GetGuildVoiceRegionsResponse) {
        return this.handleError<PylonRest.GetGuildVoiceRegionsResponse_Data, PylonRest.GetGuildVoiceRegionsResponse>(type)
    };
    GetGuildInvitesRequestTransform(type: PylonRest.GetGuildInvitesResponse) {
        return this.handleError<PylonRest.GetGuildInvitesResponse_Data, PylonRest.GetGuildInvitesResponse>(type)
    };
    ModifyChannelRequestTransform(type: PylonRest.ModifyChannelResponse) {
        return this.handleError<PylonRest.ModifyChannelResponse_Data, PylonRest.ModifyChannelResponse>(type)
    };
    DeleteChannelRequestTransform(type: PylonRest.DeleteChannelResponse) {
        return this.handleError<Empty, PylonRest.DeleteChannelResponse>(type)
    };
    GetChannelMessagesRequestTransform(type: PylonRest.GetChannelMessagesResponse) {
        return this.handleError<PylonRest.GetChannelMessagesResponse_Data, PylonRest.GetChannelMessagesResponse>(type)
    };
    GetChannelMessageRequestTransform(type: PylonRest.GetChannelMessageResponse) {
        return this.handleError<PylonRest.GetChannelMessageResponse_Data, PylonRest.GetChannelMessageResponse>(type)
    };
    CreateMessageRequestTransform(type: PylonRest.CreateMessageResponse) {
        const ret = this.handleError<PylonRest.CreateMessageResponse_Data, PylonRest.CreateMessageResponse>(type).response.data.message;
        if(ret) {
            return new Message(ret);
        }
        throw new Error("Emtpy Message returned");
    };
    CrosspostMessageRequestTransform(type: PylonRest.CrosspostMessageResponse) {
        return this.handleError<PylonRest.CrosspostMessageResponse_Data, PylonRest.CrosspostMessageResponse>(type)
    };
    CreateReactionRequestTransform(type: PylonRest.CreateReactionResponse) {
        return this.handleError<Empty, PylonRest.CreateReactionResponse>(type)
    };
    DeleteOwnReactionRequestTransform(type: PylonRest.DeleteOwnReactionResponse) {
        return this.handleError<Empty, PylonRest.DeleteOwnReactionResponse>(type)
    };
    DeleteUserReactionRequestTransform(type: PylonRest.DeleteUserReactionResponse) {
        return this.handleError<Empty, PylonRest.DeleteUserReactionResponse>(type)
    };
    DeleteAllReactionsRequestTransform(type: PylonRest.DeleteAllReactionsResponse) {
        return this.handleError<Empty, PylonRest.DeleteAllReactionsResponse>(type)
    };
    DeleteAllReactionsForEmojiRequestTransform(type: PylonRest.DeleteAllReactionsForEmojiResponse) {
        return this.handleError<Empty, PylonRest.DeleteAllReactionsForEmojiResponse>(type)
    };
    EditMessageRequestTransform(type: PylonRest.EditMessageResponse) {
        return this.handleError<PylonRest.EditMessageResponse_Data, PylonRest.EditMessageResponse>(type)
    };
    DeleteMessageRequestTransform(type: PylonRest.DeleteMessageResponse) {
        return this.handleError<Empty, PylonRest.DeleteMessageResponse>(type)
    };
    BulkDeleteMessagesRequestTransform(type: PylonRest.BulkDeleteMessagesResponse) {
        return this.handleError<Empty, PylonRest.BulkDeleteMessagesResponse>(type)
    };
    EditChannelPermissionsRequestTransform(type: PylonRest.EditChannelPermissionsResponse) {
        return this.handleError<Empty, PylonRest.EditChannelPermissionsResponse>(type)
    };
    GetChannelInvitesRequestTransform(type: PylonRest.GetChannelInvitesResponse) {
        return this.handleError<PylonRest.GetChannelInvitesResponse_Data, PylonRest.GetChannelInvitesResponse>(type)
    };
    CreateChannelInviteRequestTransform(type: PylonRest.CreateChannelInviteResponse) {
        return this.handleError<PylonRest.CreateChannelInviteResponse_Data, PylonRest.CreateChannelInviteResponse>(type)
    };
    DeleteChannelPermissionRequestTransform(type: PylonRest.DeleteChannelPermissionResponse) {
        return this.handleError<Empty, PylonRest.DeleteChannelPermissionResponse>(type)
    };
    FollowNewsChannelRequestTransform(type: PylonRest.FollowNewsChannelResponse) {
        return this.handleError<PylonRest.FollowNewsChannelResponse_Data, PylonRest.FollowNewsChannelResponse>(type)
    };
    TriggerTypingIndicatorRequestTransform(type: PylonRest.TriggerTypingIndicatorResponse) {
        return this.handleError<Empty, PylonRest.TriggerTypingIndicatorResponse>(type)
    };
    GetPinnedMessagesRequestTransform(type: PylonRest.GetPinnedMessagesResponse) {
        return this.handleError<PylonRest.GetPinnedMessagesResponse_Data, PylonRest.GetPinnedMessagesResponse>(type)
    };
    AddPinnedChannelMessageRequestTransform(type: PylonRest.AddPinnedChannelMessageResponse) {
        return this.handleError<Empty, PylonRest.AddPinnedChannelMessageResponse>(type)
    };
    DeletePinnedChannelMessageRequestTransform(type: PylonRest.DeletePinnedChannelMessageResponse) {
        return this.handleError<Empty, PylonRest.DeletePinnedChannelMessageResponse>(type)
    };
    ListGuildEmojisRequestTransform(type: PylonRest.ListGuildEmojisResponse) {
        return this.handleError<PylonRest.ListGuildEmojisResponse_Data, PylonRest.ListGuildEmojisResponse>(type)
    };
    GetGuildEmojiRequestTransform(type: PylonRest.GetGuildEmojiResponse) {
        return this.handleError<PylonRest.GetGuildEmojiResponse_Data, PylonRest.GetGuildEmojiResponse>(type)
    };
    CreateGuildEmojiRequestTransform(type: PylonRest.CreateGuildEmojiResponse) {
        return this.handleError<PylonRest.CreateGuildEmojiResponse_Data, PylonRest.CreateGuildEmojiResponse>(type)
    };
    ModifyGuildEmojiRequestTransform(type: PylonRest.ModifyGuildEmojiResponse) {
        return this.handleError<PylonRest.ModifyGuildEmojiResponse_Data, PylonRest.ModifyGuildEmojiResponse>(type)
    };
    DeleteGuildEmojiRequestTransform(type: PylonRest.DeleteGuildEmojiResponse) {
        return this.handleError<Empty, PylonRest.DeleteGuildEmojiResponse>(type)
    };
    GetCurrentUserRequestTransform(type: PylonRest.GetCurrentUserResponse) {
        return this.handleError<PylonRest.GetCurrentUserResponse_Data, PylonRest.GetCurrentUserResponse>(type)
    };
    GetUserRequestTransform(type: PylonRest.GetUserResponse) {
        return this.handleError<PylonRest.GetUserResponse_Data, PylonRest.GetUserResponse>(type)
    };
    ModifyCurrentUserRequestTransform(type: PylonRest.ModifyCurrentUserResponse) {
        return this.handleError<PylonRest.ModifyCurrentUserResponse_Data, PylonRest.ModifyCurrentUserResponse>(type)
    };
    LeaveGuildRequestTransform(type: PylonRest.LeaveGuildResponse) {
        return this.handleError<Empty, PylonRest.LeaveGuildResponse>(type)
    };
    CreateDmRequestTransform(type: PylonRest.CreateDmResponse) {
        return this.handleError<PylonRest.CreateDmResponse_Data, PylonRest.CreateDmResponse>(type)
    };
}
