import { ChannelCredentials, Metadata } from "@grpc/grpc-js";
import { EventScope } from "@pylonbot/pylon-gateway-protobuf/dist/discord/v1/event";
import * as PylonRest from "@pylonbot/pylon-gateway-protobuf/dist/discord/v1/rest";
import { GatewayRestClient } from "@pylonbot/pylon-gateway-protobuf/dist/gateway/v1/rest_service";
import { PylonMetadata } from "../structures/PylonMetadata";
import RestTransform from "./restTransform";

export default class RestClient {
	private _restClient: GatewayRestClient;
	private _transform: RestTransform;
	private _defaultMeta: PylonMetadata; 
    constructor(host: string, private botId : string) {
        this._restClient = new GatewayRestClient(
            host,
			ChannelCredentials.createInsecure(),
			{
				"grpc.max_receive_message_length": -1,
			}
		);
		this._transform = new RestTransform();
		this._defaultMeta = {
			botId,
			guildId: '0',
		}
	}

	createMetadata(meta : any) {
		const metadata = new Metadata();
		if(meta.botId) { 
			metadata.set("x-pylon-bot-id", meta.botId); 
		}
		if(meta.guildId) {
			metadata.set("x-pylon-guild-id", meta.guildId);
		}
		if(meta.shardKey) {
			metadata.set("x-pylon-shard-key", meta.shardKey);
		}
		if(meta.maxRatelimitMs) {
			metadata.set("x-pylon-max-ratelimit-ms", meta.maxRatelimitMs.toString())
		}
		return metadata;
	}

	promisifyUnaryCall<T>(originalCall: Function, request: any, metadata: any) {
		if(!metadata) {
			metadata = this._defaultMeta;
		} else {
			metadata = Object.assign({}, this._defaultMeta, metadata);
		}
		metadata = this.createMetadata(metadata);
		
		return new Promise<T>((res, rej) => {
			originalCall(request, metadata, (err: any, data: any) => {
				if (err) return rej(err);
				return res(data);
			});
		})
	}

    //DO NOT EDIT BELOW THIS LINE, THE CODE HERE WILL BE OVERWRITTEN SINCE IT IS AUTOMATICALLY GENERATED
    //{{marker_start}}
	async modifyGuild(request: PylonRest.DeepPartial<PylonRest.ModifyGuildRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.ModifyGuildRequestTransform(await this.promisifyUnaryCall<PylonRest.ModifyGuildResponse>(this._restClient.modifyGuild.bind(this._restClient), PylonRest.ModifyGuildRequest.fromPartial(request), metadata)) };
	async createGuildChannel(request: PylonRest.DeepPartial<PylonRest.CreateGuildChannelRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.CreateGuildChannelRequestTransform(await this.promisifyUnaryCall<PylonRest.CreateGuildChannelResponse>(this._restClient.createGuildChannel.bind(this._restClient), PylonRest.CreateGuildChannelRequest.fromPartial(request), metadata)) };
	async modifyGuildChannelPositions(request: PylonRest.DeepPartial<PylonRest.ModifyGuildChannelPositionsRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.ModifyGuildChannelPositionsRequestTransform(await this.promisifyUnaryCall<PylonRest.ModifyGuildChannelPositionsResponse>(this._restClient.modifyGuildChannelPositions.bind(this._restClient), PylonRest.ModifyGuildChannelPositionsRequest.fromPartial(request), metadata)) };
	async addGuildMember(request: PylonRest.DeepPartial<PylonRest.AddGuildMemberRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.AddGuildMemberRequestTransform(await this.promisifyUnaryCall<PylonRest.AddGuildMemberResponse>(this._restClient.addGuildMember.bind(this._restClient), PylonRest.AddGuildMemberRequest.fromPartial(request), metadata)) };
	async modifyGuildMember(request: PylonRest.DeepPartial<PylonRest.ModifyGuildMemberRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.ModifyGuildMemberRequestTransform(await this.promisifyUnaryCall<PylonRest.ModifyGuildMemberResponse>(this._restClient.modifyGuildMember.bind(this._restClient), PylonRest.ModifyGuildMemberRequest.fromPartial(request), metadata)) };
	async modifyCurrentUserNick(request: PylonRest.DeepPartial<PylonRest.ModifyCurrentUserNickRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.ModifyCurrentUserNickRequestTransform(await this.promisifyUnaryCall<PylonRest.ModifyCurrentUserNickResponse>(this._restClient.modifyCurrentUserNick.bind(this._restClient), PylonRest.ModifyCurrentUserNickRequest.fromPartial(request), metadata)) };
	async addGuildMemberRole(request: PylonRest.DeepPartial<PylonRest.AddGuildMemberRoleRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.AddGuildMemberRoleRequestTransform(await this.promisifyUnaryCall<PylonRest.AddGuildMemberRoleResponse>(this._restClient.addGuildMemberRole.bind(this._restClient), PylonRest.AddGuildMemberRoleRequest.fromPartial(request), metadata)) };
	async removeGuildMemberRole(request: PylonRest.DeepPartial<PylonRest.RemoveGuildMemberRoleRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.RemoveGuildMemberRoleRequestTransform(await this.promisifyUnaryCall<PylonRest.RemoveGuildMemberRoleResponse>(this._restClient.removeGuildMemberRole.bind(this._restClient), PylonRest.RemoveGuildMemberRoleRequest.fromPartial(request), metadata)) };
	async removeGuildMember(request: PylonRest.DeepPartial<PylonRest.RemoveGuildMemberRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.RemoveGuildMemberRequestTransform(await this.promisifyUnaryCall<PylonRest.RemoveGuildMemberResponse>(this._restClient.removeGuildMember.bind(this._restClient), PylonRest.RemoveGuildMemberRequest.fromPartial(request), metadata)) };
	async getGuildBans(request: PylonRest.DeepPartial<PylonRest.GetGuildBansRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetGuildBansRequestTransform(await this.promisifyUnaryCall<PylonRest.GetGuildBansResponse>(this._restClient.getGuildBans.bind(this._restClient), PylonRest.GetGuildBansRequest.fromPartial(request), metadata)) };
	async getGuildBan(request: PylonRest.DeepPartial<PylonRest.GetGuildBanRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetGuildBanRequestTransform(await this.promisifyUnaryCall<PylonRest.GetGuildBanResponse>(this._restClient.getGuildBan.bind(this._restClient), PylonRest.GetGuildBanRequest.fromPartial(request), metadata)) };
	async createGuildBan(request: PylonRest.DeepPartial<PylonRest.CreateGuildBanRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.CreateGuildBanRequestTransform(await this.promisifyUnaryCall<PylonRest.CreateGuildBanResponse>(this._restClient.createGuildBan.bind(this._restClient), PylonRest.CreateGuildBanRequest.fromPartial(request), metadata)) };
	async removeGuildBan(request: PylonRest.DeepPartial<PylonRest.RemoveGuildBanRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.RemoveGuildBanRequestTransform(await this.promisifyUnaryCall<PylonRest.RemoveGuildBanResponse>(this._restClient.removeGuildBan.bind(this._restClient), PylonRest.RemoveGuildBanRequest.fromPartial(request), metadata)) };
	async createGuildRole(request: PylonRest.DeepPartial<PylonRest.CreateGuildRoleRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.CreateGuildRoleRequestTransform(await this.promisifyUnaryCall<PylonRest.CreateGuildRoleResponse>(this._restClient.createGuildRole.bind(this._restClient), PylonRest.CreateGuildRoleRequest.fromPartial(request), metadata)) };
	async modifyGuildRolePositions(request: PylonRest.DeepPartial<PylonRest.ModifyGuildRolePositionsRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.ModifyGuildRolePositionsRequestTransform(await this.promisifyUnaryCall<PylonRest.ModifyGuildRolePositionsResponse>(this._restClient.modifyGuildRolePositions.bind(this._restClient), PylonRest.ModifyGuildRolePositionsRequest.fromPartial(request), metadata)) };
	async modifyGuildRole(request: PylonRest.DeepPartial<PylonRest.ModifyGuildRoleRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.ModifyGuildRoleRequestTransform(await this.promisifyUnaryCall<PylonRest.ModifyGuildRoleResponse>(this._restClient.modifyGuildRole.bind(this._restClient), PylonRest.ModifyGuildRoleRequest.fromPartial(request), metadata)) };
	async deleteGuildRole(request: PylonRest.DeepPartial<PylonRest.DeleteGuildRoleRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.DeleteGuildRoleRequestTransform(await this.promisifyUnaryCall<PylonRest.DeleteGuildRoleResponse>(this._restClient.deleteGuildRole.bind(this._restClient), PylonRest.DeleteGuildRoleRequest.fromPartial(request), metadata)) };
	async getGuildPruneCount(request: PylonRest.DeepPartial<PylonRest.GetGuildPruneCountRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetGuildPruneCountRequestTransform(await this.promisifyUnaryCall<PylonRest.GetGuildPruneCountResponse>(this._restClient.getGuildPruneCount.bind(this._restClient), PylonRest.GetGuildPruneCountRequest.fromPartial(request), metadata)) };
	async beginGuildPrune(request: PylonRest.DeepPartial<PylonRest.BeginGuildPruneRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.BeginGuildPruneRequestTransform(await this.promisifyUnaryCall<PylonRest.BeginGuildPruneResponse>(this._restClient.beginGuildPrune.bind(this._restClient), PylonRest.BeginGuildPruneRequest.fromPartial(request), metadata)) };
	async getGuildVoiceRegions(request: PylonRest.DeepPartial<PylonRest.GetGuildVoiceRegionsRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetGuildVoiceRegionsRequestTransform(await this.promisifyUnaryCall<PylonRest.GetGuildVoiceRegionsResponse>(this._restClient.getGuildVoiceRegions.bind(this._restClient), PylonRest.GetGuildVoiceRegionsRequest.fromPartial(request), metadata)) };
	async getGuildInvites(request: PylonRest.DeepPartial<PylonRest.GetGuildInvitesRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetGuildInvitesRequestTransform(await this.promisifyUnaryCall<PylonRest.GetGuildInvitesResponse>(this._restClient.getGuildInvites.bind(this._restClient), PylonRest.GetGuildInvitesRequest.fromPartial(request), metadata)) };
	async modifyChannel(request: PylonRest.DeepPartial<PylonRest.ModifyChannelRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.ModifyChannelRequestTransform(await this.promisifyUnaryCall<PylonRest.ModifyChannelResponse>(this._restClient.modifyChannel.bind(this._restClient), PylonRest.ModifyChannelRequest.fromPartial(request), metadata)) };
	async deleteChannel(request: PylonRest.DeepPartial<PylonRest.DeleteChannelRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.DeleteChannelRequestTransform(await this.promisifyUnaryCall<PylonRest.DeleteChannelResponse>(this._restClient.deleteChannel.bind(this._restClient), PylonRest.DeleteChannelRequest.fromPartial(request), metadata)) };
	async getChannelMessages(request: PylonRest.DeepPartial<PylonRest.GetChannelMessagesRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetChannelMessagesRequestTransform(await this.promisifyUnaryCall<PylonRest.GetChannelMessagesResponse>(this._restClient.getChannelMessages.bind(this._restClient), PylonRest.GetChannelMessagesRequest.fromPartial(request), metadata)) };
	async getChannelMessage(request: PylonRest.DeepPartial<PylonRest.GetChannelMessageRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetChannelMessageRequestTransform(await this.promisifyUnaryCall<PylonRest.GetChannelMessageResponse>(this._restClient.getChannelMessage.bind(this._restClient), PylonRest.GetChannelMessageRequest.fromPartial(request), metadata)) };
	async createMessage(request: PylonRest.DeepPartial<PylonRest.CreateMessageRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.CreateMessageRequestTransform(await this.promisifyUnaryCall<PylonRest.CreateMessageResponse>(this._restClient.createMessage.bind(this._restClient), PylonRest.CreateMessageRequest.fromPartial(request), metadata)) };
	async crosspostMessage(request: PylonRest.DeepPartial<PylonRest.CrosspostMessageRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.CrosspostMessageRequestTransform(await this.promisifyUnaryCall<PylonRest.CrosspostMessageResponse>(this._restClient.crosspostMessage.bind(this._restClient), PylonRest.CrosspostMessageRequest.fromPartial(request), metadata)) };
	async createReaction(request: PylonRest.DeepPartial<PylonRest.CreateReactionRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.CreateReactionRequestTransform(await this.promisifyUnaryCall<PylonRest.CreateReactionResponse>(this._restClient.createReaction.bind(this._restClient), PylonRest.CreateReactionRequest.fromPartial(request), metadata)) };
	async deleteOwnReaction(request: PylonRest.DeepPartial<PylonRest.DeleteOwnReactionRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.DeleteOwnReactionRequestTransform(await this.promisifyUnaryCall<PylonRest.DeleteOwnReactionResponse>(this._restClient.deleteOwnReaction.bind(this._restClient), PylonRest.DeleteOwnReactionRequest.fromPartial(request), metadata)) };
	async deleteUserReaction(request: PylonRest.DeepPartial<PylonRest.DeleteUserReactionRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.DeleteUserReactionRequestTransform(await this.promisifyUnaryCall<PylonRest.DeleteUserReactionResponse>(this._restClient.deleteUserReaction.bind(this._restClient), PylonRest.DeleteUserReactionRequest.fromPartial(request), metadata)) };
	async deleteAllReactions(request: PylonRest.DeepPartial<PylonRest.DeleteAllReactionsRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.DeleteAllReactionsRequestTransform(await this.promisifyUnaryCall<PylonRest.DeleteAllReactionsResponse>(this._restClient.deleteAllReactions.bind(this._restClient), PylonRest.DeleteAllReactionsRequest.fromPartial(request), metadata)) };
	async deleteAllReactionsForEmoji(request: PylonRest.DeepPartial<PylonRest.DeleteAllReactionsForEmojiRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.DeleteAllReactionsForEmojiRequestTransform(await this.promisifyUnaryCall<PylonRest.DeleteAllReactionsForEmojiResponse>(this._restClient.deleteAllReactionsForEmoji.bind(this._restClient), PylonRest.DeleteAllReactionsForEmojiRequest.fromPartial(request), metadata)) };
	async editMessage(request: PylonRest.DeepPartial<PylonRest.EditMessageRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.EditMessageRequestTransform(await this.promisifyUnaryCall<PylonRest.EditMessageResponse>(this._restClient.editMessage.bind(this._restClient), PylonRest.EditMessageRequest.fromPartial(request), metadata)) };
	async deleteMessage(request: PylonRest.DeepPartial<PylonRest.DeleteMessageRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.DeleteMessageRequestTransform(await this.promisifyUnaryCall<PylonRest.DeleteMessageResponse>(this._restClient.deleteMessage.bind(this._restClient), PylonRest.DeleteMessageRequest.fromPartial(request), metadata)) };
	async bulkDeleteMessages(request: PylonRest.DeepPartial<PylonRest.BulkDeleteMessagesRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.BulkDeleteMessagesRequestTransform(await this.promisifyUnaryCall<PylonRest.BulkDeleteMessagesResponse>(this._restClient.bulkDeleteMessages.bind(this._restClient), PylonRest.BulkDeleteMessagesRequest.fromPartial(request), metadata)) };
	async editChannelPermissions(request: PylonRest.DeepPartial<PylonRest.EditChannelPermissionsRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.EditChannelPermissionsRequestTransform(await this.promisifyUnaryCall<PylonRest.EditChannelPermissionsResponse>(this._restClient.editChannelPermissions.bind(this._restClient), PylonRest.EditChannelPermissionsRequest.fromPartial(request), metadata)) };
	async getChannelInvites(request: PylonRest.DeepPartial<PylonRest.GetChannelInvitesRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetChannelInvitesRequestTransform(await this.promisifyUnaryCall<PylonRest.GetChannelInvitesResponse>(this._restClient.getChannelInvites.bind(this._restClient), PylonRest.GetChannelInvitesRequest.fromPartial(request), metadata)) };
	async createChannelInvite(request: PylonRest.DeepPartial<PylonRest.CreateChannelInviteRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.CreateChannelInviteRequestTransform(await this.promisifyUnaryCall<PylonRest.CreateChannelInviteResponse>(this._restClient.createChannelInvite.bind(this._restClient), PylonRest.CreateChannelInviteRequest.fromPartial(request), metadata)) };
	async deleteChannelPermission(request: PylonRest.DeepPartial<PylonRest.DeleteChannelPermissionRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.DeleteChannelPermissionRequestTransform(await this.promisifyUnaryCall<PylonRest.DeleteChannelPermissionResponse>(this._restClient.deleteChannelPermission.bind(this._restClient), PylonRest.DeleteChannelPermissionRequest.fromPartial(request), metadata)) };
	async followNewsChannel(request: PylonRest.DeepPartial<PylonRest.FollowNewsChannelRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.FollowNewsChannelRequestTransform(await this.promisifyUnaryCall<PylonRest.FollowNewsChannelResponse>(this._restClient.followNewsChannel.bind(this._restClient), PylonRest.FollowNewsChannelRequest.fromPartial(request), metadata)) };
	async triggerTypingIndicator(request: PylonRest.DeepPartial<PylonRest.TriggerTypingIndicatorRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.TriggerTypingIndicatorRequestTransform(await this.promisifyUnaryCall<PylonRest.TriggerTypingIndicatorResponse>(this._restClient.triggerTypingIndicator.bind(this._restClient), PylonRest.TriggerTypingIndicatorRequest.fromPartial(request), metadata)) };
	async getPinnedMessages(request: PylonRest.DeepPartial<PylonRest.GetPinnedMessagesRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetPinnedMessagesRequestTransform(await this.promisifyUnaryCall<PylonRest.GetPinnedMessagesResponse>(this._restClient.getPinnedMessages.bind(this._restClient), PylonRest.GetPinnedMessagesRequest.fromPartial(request), metadata)) };
	async addPinnedChannelMessage(request: PylonRest.DeepPartial<PylonRest.AddPinnedChannelMessageRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.AddPinnedChannelMessageRequestTransform(await this.promisifyUnaryCall<PylonRest.AddPinnedChannelMessageResponse>(this._restClient.addPinnedChannelMessage.bind(this._restClient), PylonRest.AddPinnedChannelMessageRequest.fromPartial(request), metadata)) };
	async deletePinnedChannelMessage(request: PylonRest.DeepPartial<PylonRest.DeletePinnedChannelMessageRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.DeletePinnedChannelMessageRequestTransform(await this.promisifyUnaryCall<PylonRest.DeletePinnedChannelMessageResponse>(this._restClient.deletePinnedChannelMessage.bind(this._restClient), PylonRest.DeletePinnedChannelMessageRequest.fromPartial(request), metadata)) };
	async listGuildEmojis(request: PylonRest.DeepPartial<PylonRest.ListGuildEmojisRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.ListGuildEmojisRequestTransform(await this.promisifyUnaryCall<PylonRest.ListGuildEmojisResponse>(this._restClient.listGuildEmojis.bind(this._restClient), PylonRest.ListGuildEmojisRequest.fromPartial(request), metadata)) };
	async getGuildEmoji(request: PylonRest.DeepPartial<PylonRest.GetGuildEmojiRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetGuildEmojiRequestTransform(await this.promisifyUnaryCall<PylonRest.GetGuildEmojiResponse>(this._restClient.getGuildEmoji.bind(this._restClient), PylonRest.GetGuildEmojiRequest.fromPartial(request), metadata)) };
	async createGuildEmoji(request: PylonRest.DeepPartial<PylonRest.CreateGuildEmojiRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.CreateGuildEmojiRequestTransform(await this.promisifyUnaryCall<PylonRest.CreateGuildEmojiResponse>(this._restClient.createGuildEmoji.bind(this._restClient), PylonRest.CreateGuildEmojiRequest.fromPartial(request), metadata)) };
	async modifyGuildEmoji(request: PylonRest.DeepPartial<PylonRest.ModifyGuildEmojiRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.ModifyGuildEmojiRequestTransform(await this.promisifyUnaryCall<PylonRest.ModifyGuildEmojiResponse>(this._restClient.modifyGuildEmoji.bind(this._restClient), PylonRest.ModifyGuildEmojiRequest.fromPartial(request), metadata)) };
	async deleteGuildEmoji(request: PylonRest.DeepPartial<PylonRest.DeleteGuildEmojiRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.DeleteGuildEmojiRequestTransform(await this.promisifyUnaryCall<PylonRest.DeleteGuildEmojiResponse>(this._restClient.deleteGuildEmoji.bind(this._restClient), PylonRest.DeleteGuildEmojiRequest.fromPartial(request), metadata)) };
	async getCurrentUser(request: PylonRest.DeepPartial<PylonRest.GetCurrentUserRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetCurrentUserRequestTransform(await this.promisifyUnaryCall<PylonRest.GetCurrentUserResponse>(this._restClient.getCurrentUser.bind(this._restClient), PylonRest.GetCurrentUserRequest.fromPartial(request), metadata)) };
	async getUser(request: PylonRest.DeepPartial<PylonRest.GetUserRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetUserRequestTransform(await this.promisifyUnaryCall<PylonRest.GetUserResponse>(this._restClient.getUser.bind(this._restClient), PylonRest.GetUserRequest.fromPartial(request), metadata)) };
	async modifyCurrentUser(request: PylonRest.DeepPartial<PylonRest.ModifyCurrentUserRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.ModifyCurrentUserRequestTransform(await this.promisifyUnaryCall<PylonRest.ModifyCurrentUserResponse>(this._restClient.modifyCurrentUser.bind(this._restClient), PylonRest.ModifyCurrentUserRequest.fromPartial(request), metadata)) };
	async leaveGuild(request: PylonRest.DeepPartial<PylonRest.LeaveGuildRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.LeaveGuildRequestTransform(await this.promisifyUnaryCall<PylonRest.LeaveGuildResponse>(this._restClient.leaveGuild.bind(this._restClient), PylonRest.LeaveGuildRequest.fromPartial(request), metadata)) };
	async createDm(request: PylonRest.DeepPartial<PylonRest.CreateDmRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.CreateDmRequestTransform(await this.promisifyUnaryCall<PylonRest.CreateDmResponse>(this._restClient.createDm.bind(this._restClient), PylonRest.CreateDmRequest.fromPartial(request), metadata)) };
	//{{marker_end}}
    //Anything below this is safe
}

