import { ChannelCredentials, Metadata } from "@grpc/grpc-js";
import * as PylonCache from "@pylonbot/pylon-gateway-protobuf/dist/discord/v1/cache";
import { EventScope } from "@pylonbot/pylon-gateway-protobuf/dist/discord/v1/event";
import { GatewayCacheClient } from "@pylonbot/pylon-gateway-protobuf/dist/gateway/v1/cache_service";
import createMetadata from 'grpc-create-metadata';
import { PylonMetadata } from "../structures/PylonMetadata";
import CacheTransform from "./cacheTransform";

export default class CacheClient {
    private _restClient: GatewayCacheClient;
    private _transform: CacheTransform;
    private _defaultMeta: PylonMetadata;
    constructor(host: string, private botId: string) {
        this._restClient = new GatewayCacheClient(
            host,
			ChannelCredentials.createInsecure(),
			{
				"grpc.max_receive_message_length": -1,
			}
        );
        this._transform = new CacheTransform();
        this._defaultMeta = {
			botId,
		}
    }

    createMetadata(meta : any) {
		const metadata = new Metadata();
		if(meta.botId) { 
			metadata.set("x-pylon-bot-id", meta.botId); 
		}
		if(meta.guildId) {
			metadata.set("x-pylon-guild-id", meta.guildId);
		}
		if(meta.shardKey) {
			metadata.set("x-pylon-shard-key", meta.shardKey);
		}
		return metadata;
	}

    promisifyUnaryCall<T>(originalCall: Function, request: any, metadata: any) {
		if(request.$type === "pylon.discord.v1.cache.ListGuildMembersRequest" || request.$type === "pylon.discord.v1.cache.FindGuildMembersRequest") {
			if(request.limit === 0) {
				request.limit = -1;
			}
		}

		if(!metadata) {
			metadata = this._defaultMeta;
		} else {
			metadata = Object.assign({}, this._defaultMeta, metadata);
		}
		metadata = this.createMetadata(metadata);
    
        return new Promise<T>((res, rej) => {
            originalCall(request, metadata, (err: any, data: any) => {
                if (err) return rej(err);
                return res(data);
            });
        })
    }

    //DO NOT EDIT BELOW THIS LINE, THE CODE HERE WILL BE OVERWRITTEN SINCE IT IS AUTOMATICALLY GENERATED
    //{{marker_start}}
	async getGuild(request: PylonCache.DeepPartial<PylonCache.GetGuildRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetGuildRequestTransform(await this.promisifyUnaryCall<PylonCache.GetGuildResponse>(this._restClient.getGuild.bind(this._restClient), PylonCache.GetGuildRequest.fromPartial(request), metadata)) };
	async listGuildChannels(request: PylonCache.DeepPartial<PylonCache.ListGuildChannelsRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.ListGuildChannelsRequestTransform(await this.promisifyUnaryCall<PylonCache.ListGuildChannelsResponse>(this._restClient.listGuildChannels.bind(this._restClient), PylonCache.ListGuildChannelsRequest.fromPartial(request), metadata)) };
	async getGuildChannel(request: PylonCache.DeepPartial<PylonCache.GetGuildChannelRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetGuildChannelRequestTransform(await this.promisifyUnaryCall<PylonCache.GetGuildChannelResponse>(this._restClient.getGuildChannel.bind(this._restClient), PylonCache.GetGuildChannelRequest.fromPartial(request), metadata)) };
	async listGuildMembers(request: PylonCache.DeepPartial<PylonCache.ListGuildMembersRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.ListGuildMembersRequestTransform(await this.promisifyUnaryCall<PylonCache.ListGuildMembersResponse>(this._restClient.listGuildMembers.bind(this._restClient), PylonCache.ListGuildMembersRequest.fromPartial(request), metadata)) };
	async getGuildMember(request: PylonCache.DeepPartial<PylonCache.GetGuildMemberRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetGuildMemberRequestTransform(await this.promisifyUnaryCall<PylonCache.GetGuildMemberResponse>(this._restClient.getGuildMember.bind(this._restClient), PylonCache.GetGuildMemberRequest.fromPartial(request), metadata)) };
	async findGuildMembers(request: PylonCache.DeepPartial<PylonCache.FindGuildMembersRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.FindGuildMembersRequestTransform(await this.promisifyUnaryCall<PylonCache.FindGuildMembersResponse>(this._restClient.findGuildMembers.bind(this._restClient), PylonCache.FindGuildMembersRequest.fromPartial(request), metadata)) };
	async getGuildMemberPresence(request: PylonCache.DeepPartial<PylonCache.GetGuildMemberPresenceRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetGuildMemberPresenceRequestTransform(await this.promisifyUnaryCall<PylonCache.GetGuildMemberPresenceResponse>(this._restClient.getGuildMemberPresence.bind(this._restClient), PylonCache.GetGuildMemberPresenceRequest.fromPartial(request), metadata)) };
	async listGuildRoles(request: PylonCache.DeepPartial<PylonCache.ListGuildRolesRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.ListGuildRolesRequestTransform(await this.promisifyUnaryCall<PylonCache.ListGuildRolesResponse>(this._restClient.listGuildRoles.bind(this._restClient), PylonCache.ListGuildRolesRequest.fromPartial(request), metadata)) };
	async getGuildRole(request: PylonCache.DeepPartial<PylonCache.GetGuildRoleRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetGuildRoleRequestTransform(await this.promisifyUnaryCall<PylonCache.GetGuildRoleResponse>(this._restClient.getGuildRole.bind(this._restClient), PylonCache.GetGuildRoleRequest.fromPartial(request), metadata)) };
	async listGuildEmojis(request: PylonCache.DeepPartial<PylonCache.ListGuildEmojisRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.ListGuildEmojisRequestTransform(await this.promisifyUnaryCall<PylonCache.ListGuildEmojisResponse>(this._restClient.listGuildEmojis.bind(this._restClient), PylonCache.ListGuildEmojisRequest.fromPartial(request), metadata)) };
	async getGuildEmoji(request: PylonCache.DeepPartial<PylonCache.GetGuildEmojiRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetGuildEmojiRequestTransform(await this.promisifyUnaryCall<PylonCache.GetGuildEmojiResponse>(this._restClient.getGuildEmoji.bind(this._restClient), PylonCache.GetGuildEmojiRequest.fromPartial(request), metadata)) };
	async getGuildMemberVoiceState(request: PylonCache.DeepPartial<PylonCache.GetGuildMemberVoiceStateRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetGuildMemberVoiceStateRequestTransform(await this.promisifyUnaryCall<PylonCache.GetGuildMemberVoiceStateResponse>(this._restClient.getGuildMemberVoiceState.bind(this._restClient), PylonCache.GetGuildMemberVoiceStateRequest.fromPartial(request), metadata)) };
	async listGuildChannelVoiceStates(request: PylonCache.DeepPartial<PylonCache.ListGuildChannelVoiceStatesRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.ListGuildChannelVoiceStatesRequestTransform(await this.promisifyUnaryCall<PylonCache.ListGuildChannelVoiceStatesResponse>(this._restClient.listGuildChannelVoiceStates.bind(this._restClient), PylonCache.ListGuildChannelVoiceStatesRequest.fromPartial(request), metadata)) };
	async getUser(request: PylonCache.DeepPartial<PylonCache.GetUserRequest>, metadata?: PylonMetadata | EventScope | undefined) { return this._transform.GetUserRequestTransform(await this.promisifyUnaryCall<PylonCache.GetUserResponse>(this._restClient.getUser.bind(this._restClient), PylonCache.GetUserRequest.fromPartial(request), metadata)) };
	//{{marker_end}}
    //Anything below this is safe
}

