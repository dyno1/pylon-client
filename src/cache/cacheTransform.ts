import * as PylonCache from "@pylonbot/pylon-gateway-protobuf/dist/discord/v1/cache";
import Member from '../structures/Member';
import User from '../structures/User';

export default class CacheTransform {
    GetGuildRequestTransform(type : PylonCache.GetGuildResponse) { return type; }
    ListGuildChannelsRequestTransform(type : PylonCache.ListGuildChannelsResponse) { return type; }
    GetGuildChannelRequestTransform(type : PylonCache.GetGuildChannelResponse) { return type; }
    ListGuildMembersRequestTransform(type : PylonCache.ListGuildMembersResponse) { return type.members.map(m => new Member(m)); }
    GetGuildMemberRequestTransform(type : PylonCache.GetGuildMemberResponse) { return type?.member && new Member(type.member); }
    FindGuildMembersRequestTransform(type : PylonCache.FindGuildMembersResponse) { return type.members.map(m => new Member(m)); }
    GetGuildMemberPresenceRequestTransform(type : PylonCache.GetGuildMemberPresenceResponse) { return type; }
    ListGuildRolesRequestTransform(type : PylonCache.ListGuildRolesResponse) { return type; }
    GetGuildRoleRequestTransform(type : PylonCache.GetGuildRoleResponse) { return type; }
    ListGuildEmojisRequestTransform(type : PylonCache.ListGuildEmojisResponse) { return type; }
    GetGuildEmojiRequestTransform(type : PylonCache.GetGuildEmojiResponse) { return type; }
    GetGuildMemberVoiceStateRequestTransform(type : PylonCache.GetGuildMemberVoiceStateResponse) { return type; }
    ListGuildChannelVoiceStatesRequestTransform(type : PylonCache.ListGuildChannelVoiceStatesResponse) { return type; }
    GetUserRequestTransform(type : PylonCache.GetUserResponse) { return type?.user && new User(type.user); }
}