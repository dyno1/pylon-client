import { ChannelCredentials, Metadata } from "@grpc/grpc-js";
import * as PylonGateway from "@pylonbot/pylon-gateway-protobuf/dist/discord/v1/gateway";
import createMetadata from 'grpc-create-metadata';
import { GatewayClient } from "@pylonbot/pylon-gateway-protobuf/dist/gateway/v1/gateway_service";
import { PylonMetadata } from "../structures/PylonMetadata";
import { EventScope } from "@pylonbot/pylon-gateway-protobuf/dist/discord/v1/event";

export default class GatewayAPIClient {
    private _restClient: GatewayClient;
    private _defaultMeta: PylonMetadata;
    constructor(host: string, private botId: string) {
        this._restClient = new GatewayClient(
            host,
			ChannelCredentials.createInsecure(),
			{
				"grpc.max_receive_message_length": -1,
			}
        );
        this._defaultMeta = {
			botId,
		}
    }

    createMetadata(meta : any) {
		const metadata = new Metadata();
		if(meta.botId) { 
			metadata.set("x-pylon-bot-id", meta.botId); 
		}
		if(meta.guildId) {
			metadata.set("x-pylon-guild-id", meta.guildId);
		}
		if(meta.shardKey) {
			metadata.set("x-pylon-shard-key", meta.shardKey);
		}
		return metadata;
	}

    promisifyUnaryCall<T>(originalCall: Function, request: any, metadata: any) {
		if(!metadata) {
			metadata = this._defaultMeta;
		} else {
			metadata = Object.assign({}, this._defaultMeta, metadata);
		}
		metadata = this.createMetadata(metadata);
    
        return new Promise<T>((res, rej) => {
            originalCall(request, metadata, (err: any, data: any) => {
                if (err) return rej(err);
                return res(data);
            });
        })
    }

    //DO NOT EDIT BELOW THIS LINE, THE CODE HERE WILL BE OVERWRITTEN SINCE IT IS AUTOMATICALLY GENERATED
    //{{marker_start}}
	async updateVoiceState(request: Partial<PylonGateway.UpdateVoiceStateRequest>, metadata?: PylonMetadata | EventScope | undefined) { return await this.promisifyUnaryCall<PylonGateway.UpdateVoiceStateResponse>(this._restClient.updateVoiceState.bind(this._restClient), PylonGateway.UpdateVoiceStateRequest.fromPartial(request), metadata) };
	async updateStatus(request: Partial<PylonGateway.UpdateStatusRequest>, metadata?: PylonMetadata | EventScope | undefined) { return await this.promisifyUnaryCall<PylonGateway.UpdateStatusResponse>(this._restClient.updateStatus.bind(this._restClient), PylonGateway.UpdateStatusRequest.fromPartial(request), metadata) };
	async findUser(request: Partial<PylonGateway.FindUserRequest>, metadata?: PylonMetadata | EventScope | undefined) { return await this.promisifyUnaryCall<PylonGateway.FindUserResponse>(this._restClient.findUser.bind(this._restClient), PylonGateway.FindUserRequest.fromPartial(request), metadata) };
	async findUserMutualGuilds(request: Partial<PylonGateway.GetUserMutualGuildsRequest>, metadata?: PylonMetadata | EventScope | undefined) { return await this.promisifyUnaryCall<PylonGateway.GetUserMutualGuildsResponse>(this._restClient.findUserMutualGuilds.bind(this._restClient), PylonGateway.GetUserMutualGuildsRequest.fromPartial(request), metadata) };
	async findEmoji(request: Partial<PylonGateway.FindEmojiRequest>, metadata?: PylonMetadata | EventScope | undefined) { return await this.promisifyUnaryCall<PylonGateway.FindEmojiResponse>(this._restClient.findEmoji.bind(this._restClient), PylonGateway.FindEmojiRequest.fromPartial(request), metadata) };
	async getStats(request: Partial<PylonGateway.GetStatsRequest>, metadata?: PylonMetadata | EventScope | undefined) { return await this.promisifyUnaryCall<PylonGateway.GetStatsResponse>(this._restClient.getStats.bind(this._restClient), PylonGateway.GetStatsRequest.fromPartial(request), metadata) };
	//{{marker_end}}
    //Anything below this is safe
}

