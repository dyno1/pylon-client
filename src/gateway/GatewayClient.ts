import EventEmitter from 'events';
import {
	ChannelCredentials,
	ClientDuplexStream,
	Metadata,
} from '@grpc/grpc-js';

import {
	WorkerStreamClientMessage,
	WorkerStreamServerMessage,
} from '@pylonbot/pylon-gateway-protobuf/dist/gateway/v1/workergroup';

import { GatewayWorkerGroupClient } from '@pylonbot/pylon-gateway-protobuf/dist/gateway/v1/workergroup_service';
import os from 'os';

export interface WorkerGroupClientOptions {
	events?: string[];
	host?: string;
	port?: number;
	meta?: {[key: string]: string};
}

export default class GatewayClient extends EventEmitter {
	readonly consumerGroup: string;
	readonly consumerId: string;
	readonly dsn: string;

	private eventTypes: string[];
	private client: GatewayWorkerGroupClient;
	private options: WorkerGroupClientOptions;
	private stream:
		| ClientDuplexStream<WorkerStreamClientMessage, WorkerStreamServerMessage>
		| undefined;


	private drained = false;
	private drainResolve?: Function;

	private sequence?: string;
	private routerTicket?: string;

	constructor(dsn: string, options: WorkerGroupClientOptions = {}) {
		super();
		// todo: derive fields from dsn string
		this.dsn = dsn;
		this.consumerGroup = 'test';
		this.consumerId = os.hostname();
		this.options = options;
		this.eventTypes = options.events || [];

		this.client = new GatewayWorkerGroupClient(
			`${options.host ?? 'localhost'}:${options.port ?? 8077}`,
			ChannelCredentials.createInsecure()
		);

		this.installSignalHandler();
	}

	public connect() {
		if (this.drained) {
			// we shouldn't connect to a drained stream
			return;
		}

		const meta = new Metadata();

		// you can call .add() multiple times per key
		if (this.options.meta) {
			for (const [key, value] of Object.entries(this.options.meta)) {
				meta.add(key, value);
			}
		}

		for (let type of this.eventTypes) {
			meta.add('x-pylon-event-types', type);
		}

		const stream = this.client.workerStream(meta);
		this.stream = stream;

		stream.once('close', () => {
			this.emit('debug', `Stream closed - drained: ${this.drained}`);
			this.emit('disconnect', { drained: this.drained });
			this.stream = undefined;
			if (this.drainResolve) {
				this.drainResolve();
			}
			setTimeout(() => {
				this.connect();
			}, 5000);
		});

		stream.once('error', (e: any) => {
			stream.destroy();
			console.log(e);
			this.emit('error', e);
		});

		stream.on('data', (data: WorkerStreamServerMessage) => {
			switch (data.payload?.$case) {
				case 'identifyResponse':
					this.routerTicket = data.payload.identifyResponse.routerTicket;
					this.emit('identifyResponse', data.payload.identifyResponse);
					this.emit('info', `Authenticated! routerTicket: ${this.routerTicket}`);
					break;
				case 'eventEnvelope':
					this.sequence = data.payload.eventEnvelope.header?.seq;
					const event = data.payload.eventEnvelope;
					switch (event.eventData?.$case) {
						case 'guildCreateEvent':
							this.emit('guildCreateEvent', event.eventData.guildCreateEvent);
							break;
						case 'guildUpdateEvent':
							this.emit('guildUpdateEvent', event.eventData.guildUpdateEvent);
							break;
						case 'guildDeleteEvent':
							this.emit('guildDeleteEvent', event.eventData.guildDeleteEvent);
							break;
						case 'presenceUpdateEvent':
							this.emit('presenceUpdateEvent', event.eventData.presenceUpdateEvent);
							break;
						case 'guildMemberAddEvent':
							this.emit('guildMemberAddEvent', event.eventData.guildMemberAddEvent);
							break;
						case 'guildMemberUpdateEvent':
							this.emit('guildMemberUpdateEvent', event.eventData.guildMemberUpdateEvent);
							break;
						case 'guildMemberRemoveEvent':
							this.emit('guildMemberRemoveEvent', event.eventData.guildMemberRemoveEvent);
							break;
						case 'channelCreateEvent':
							this.emit('channelCreateEvent', event.eventData.channelCreateEvent);
							break;
						case 'channelUpdateEvent':
							this.emit('channelUpdateEvent', event.eventData.channelUpdateEvent);
							break;
						case 'channelDeleteEvent':
							this.emit('channelDeleteEvent', event.eventData.channelDeleteEvent);
							break;
						case 'channelPinsUpdateEvent':
							this.emit('channelPinsUpdateEvent', event.eventData.channelPinsUpdateEvent);
							break;
						case 'guildRoleCreateEvent':
							this.emit('guildRoleCreateEvent', event.eventData.guildRoleCreateEvent);
							break;
						case 'guildRoleUpdateEvent':
							this.emit('guildRoleUpdateEvent', event.eventData.guildRoleUpdateEvent);
							break;
						case 'guildRoleDeleteEvent':
							this.emit('guildRoleDeleteEvent', event.eventData.guildRoleDeleteEvent);
							break;
						case 'messageCreateEvent':
							this.emit('messageCreateEvent', event.eventData.messageCreateEvent);
							break;
						case 'messageUpdateEvent':
							this.emit('messageUpdateEvent', event.eventData.messageUpdateEvent);
							break;
						case 'messageDeleteEvent':
							this.emit('messageDeleteEvent', event.eventData.messageDeleteEvent);
							break;
						case 'messageDeleteBulkEvent':
							this.emit('messageDeleteBulkEvent', event.eventData.messageDeleteBulkEvent);
							break;
						case 'messageReactionAddEvent':
							this.emit('messageReactionAddEvent', event.eventData.messageReactionAddEvent);
							break;
						case 'messageReactionRemoveEvent':
							this.emit('messageReactionRemoveEvent', event.eventData.messageReactionRemoveEvent);
							break;
						case 'messageReactionRemoveAllEvent':
							this.emit('messageReactionRemoveAllEvent', event.eventData.messageReactionRemoveAllEvent);
							break;
						case 'messageReactionRemoveEmojiEvent':
							this.emit('messageReactionRemoveEmojiEvent', event.eventData.messageReactionRemoveEmojiEvent);
							break;
						case 'typingStartEvent':
							this.emit('typingStartEvent', event.eventData.typingStartEvent);
							break;
						case 'voiceStateUpdateEvent':
							this.emit('voiceStateUpdateEvent', event.eventData.voiceStateUpdateEvent);
							break;
						case 'voiceServerUpdateEvent':
							this.emit('voiceServerUpdateEvent', event.eventData.voiceServerUpdateEvent);
							break;
						case 'inviteCreateEvent':
							this.emit('inviteCreateEvent', event.eventData.inviteCreateEvent);
							break;
						case 'inviteDeleteEvent':
							this.emit('inviteDeleteEvent', event.eventData.inviteDeleteEvent);
							break;
						case 'guildBanAddEvent':
							this.emit('guildBanAddEvent', event.eventData.guildBanAddEvent);
							break;
						case 'guildBanRemoveEvent':
							this.emit('guildBanRemoveEvent', event.eventData.guildBanRemoveEvent);
							break;
						case 'guildEmojisUpdateEvent':
							this.emit('guildEmojisUpdateEvent', event.eventData.guildEmojisUpdateEvent);
							break;
						case 'guildIntegrationsUpdateEvent':
							this.emit('guildIntegrationsUpdateEvent', event.eventData.guildIntegrationsUpdateEvent);
							break;
						case 'webhooksUpdateEvent':
							this.emit('webhooksUpdateEvent', event.eventData.webhooksUpdateEvent);
							break;
						case 'integrationCreateEvent':
							this.emit('integrationCreateEvent', event.eventData.integrationCreateEvent);
							break;
						case 'integrationUpdateEvent':
							this.emit('integrationUpdateEvent', event.eventData.integrationUpdateEvent);
							break;
						case 'integrationDeleteEvent':
							this.emit('integrationDeleteEvent', event.eventData.integrationDeleteEvent);
							break;
						case 'interactionCreateEvent':
							this.emit('interactionCreateEvent', event.eventData.interactionCreateEvent);
							break;
						case 'readyEvent':
							this.emit('readyEvent', event.eventData.readyEvent);
							break;
						case 'resumeEvent':
							this.emit('resumeEvent', event.eventData.resumeEvent);
							break;
						default:
							this.emit('unknownEvent', event.eventData);
							break;
					}
					break;
				case 'streamClosed':
					this.emit('debug', `received graceful close: ${data.payload.streamClosed.reason}`);
					stream.destroy();
					this.emit('streamClosed');
					break;
				case "heartbeatRequest":
					const { nonce } = data.payload.heartbeatRequest;
					console.log('heartbeat req received', data.payload);
					stream.write(
						WorkerStreamClientMessage.fromPartial({
						payload: {
							$case: "heartbeatAck",
							heartbeatAck: {
							nonce,
							sequence: this.sequence,
							},
						},
						})
					);
					break;
			}
		});

		this.emit('info', 'Authenticating...');
		stream.write(
			WorkerStreamClientMessage.fromPartial({
				payload: {
					$case: 'identifyRequest',
					identifyRequest: {
						authToken: 'noauth', // todo: read params from connection string
						lastSequence: this.sequence,
						consumerGroup: this.consumerGroup,
						consumerId: this.consumerId,
						// fine if undefined
						routerTicket: this.routerTicket
					},
				},
			})
		);
	}

	private installSignalHandler() {
		const handleSignal = async () => {
			this.emit('info', 'received SIGTERM|SIGINT signal, draining + waiting 30s for shutdown');

			setTimeout(() => {
				this.emit('debug', 'waited 30s, shutting down');
				process.exit(1);
			}, 30000);
	
			this.emit('drain');
			await this.drain();
			
			process.exit(0);
		};
		process.on('SIGINT', handleSignal)
		process.on('SIGTERM', handleSignal);
	  }
	
	public async drain() {
		if (!this.stream || this.drained) {
			this.emit('info', 'request to drain non-ready stream ignored');
			return;
		}

		this.drained = true;
		const seq = this.sequence || 0;

		this.emit('info', `draining stream, sequence: ${seq}`);

		const drainPromise = new Promise((r) => (this.drainResolve = r));
		this.stream.write(
			WorkerStreamClientMessage.fromPartial({
				payload: {
					$case: 'drainRequest',
					drainRequest: {
					sequence: `${seq}`,
					},
				},
			})
		);

		await drainPromise;

		this.emit('info', 'drain complete');
	}
}
